class ProjectSearch {
    
    clickFiltersButton() {
        return cy.get('.filters-toggle-btn').click();
    }

    getFilters(){
        return cy.get('.shot-filters')
    }

    focusFilterInput() {
        return cy.get('#tag').focus();
    }

    applyFilters() {
        cy.get('#tag').type('dog').type('{enter}')
            .get('.find-shots-timeframe').click()
            .get('.timeframe-month').click()
        return cy.get('.meatball')
    }

    clearFilter() {
        cy.get('a[data-param="tag"]').click()
        return cy.get('.meatball')
    }

    clickProject() {
        return cy.get('#screenshot-15164906').click()
    }

    getOverlay() {
        return cy.get('.shot-overlay')
    }
}

export default ProjectSearch