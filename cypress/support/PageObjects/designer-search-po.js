class DesignerSearch {

    scrollDesignerList() {
        return cy.get('.footer-main-content').scrollIntoView()
    }

    modalClick() {
        return cy.get('.action-indicator > svg').click()
    }

    getModal(){
        return cy.get('#upsell-modal')
    }

    getBody(){
        return cy.get('#designers')
    }
}

export default DesignerSearch