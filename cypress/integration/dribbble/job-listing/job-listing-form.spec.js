import JobListingForm from '../../../support/PageObjects/job-listing-form-po'
import data from '../../../fixtures/jobForm.json'

describe('Creating new job listing', () => {

    beforeEach(() => {
        cy.dribbleVisit('jobs/new');
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
    })

    const jobListingForm = new JobListingForm

    it('if all mandatory fields are blank, error message is visible', () => {
        jobListingForm.clickSubmitButton()
        jobListingForm.getErrorMessage()
            .should('be.visible')
    })

    it('if all mandatory fields are filled in, form is successfully sent', () => {
        jobListingForm.jobTitleType(data.form.jobTitle)
        jobListingForm.companyType(data.form.company)
        jobListingForm.jobDescType(data.form.jobDescription)
        jobListingForm.jobLocType(data.form.jobLocation)
        jobListingForm.linkToApplyType(data.form.link)
        jobListingForm.clickSubmitButton()
        jobListingForm.getPreview()
            .should('be.visible')
    })


    it('clicking on Cancel button redirects to /jobs page', () => {
        jobListingForm.clickCancelButton()
        cy.url()
            .should('eq', 'https://dribbble.com/jobs')
    })
})