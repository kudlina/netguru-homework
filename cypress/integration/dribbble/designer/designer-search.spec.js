import DesignerSearch from '../../../support/PageObjects/designer-search-po'

describe('Designer search for not logged in user', () => {

    before(() => {
        cy.dribbleVisit('designers');
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
    })
    const designerSearch = new DesignerSearch

    //to be double-checked
    it('after scrolling down the list of designers the subscribe overlay is visible', () => {
        designerSearch.scrollDesignerList()
        designerSearch.getModal()
            .should('be.visible')
    })

    it('when subscribe overlay is visible, user cannot scroll through page', () => {
        designerSearch.getBody()
            .should('have.class', 'noscroll')
    })

    it('when subscribe overlay is closed, user can scroll through page', () => {
        designerSearch.modalClick()
        designerSearch.getBody()
            .should('not.have.class', 'noscroll')
    })

})