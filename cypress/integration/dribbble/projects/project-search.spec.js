import ProjectSearch from '../../../support/PageObjects/project-search-po';

describe('Search filters', () => {

    before(() => {
        cy.dribbleVisit('');
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
        })
    })

    const projectSearch = new ProjectSearch();

    it('clicking on Filters button expands filter options', () => {
        projectSearch.clickFiltersButton()
        projectSearch.getFilters()
            .should('be.visible')
    })

    it('on mouse focus Tags filter block has changed border color', () => {
        projectSearch.focusFilterInput()
            .should('have.css', 'border-color', 'rgba(234, 76, 137, 0.4)')
    })


    //this test fails even though it's written correctly; bug detected on page
    it('number of applied filters is reflected by number icon', () => {
        projectSearch.applyFilters()
            .should('have.text', '2')
    })


    //this test is false-positive because of the bug (see above)
    it('clearing filters decrements number in icon', () => {
        projectSearch.clearFilter()
            .should('have.text', '1')
    })

    it('clicking on project opens overlay', () => {
        projectSearch.clickProject()
        projectSearch.getOverlay()
            .should('have.class', 'overlay-visible')
    })
})