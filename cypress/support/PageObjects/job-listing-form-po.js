class JobListingForm {

    getErrorMessage() {
        return cy.get('.errorExplanation')
    }

    clickSubmitButton() {
        return cy.get('.form-btns > .form-sub').click()
    }

    jobTitleType(text) {
        return cy.get('#job_title').type(text)
    }

    companyType(text) {
        return cy.get('#job_organization_name').type(text)
    }

    jobDescType(text) {
        return cy.get('.ql-editor').type(text)
    }

    jobLocType(text) {
        return cy.get('#job_location').type(text)
    }

    linkToApplyType(text) {
        return cy.get('#job_url').type(text)
    }

    clickCancelButton() {
        return cy.get('.form-btn').click()
    }

    getPreview() {
        return cy.get('.job-show-preview-container')
    }

}

export default JobListingForm