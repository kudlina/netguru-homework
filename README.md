# README #

This README documents progress of writing UI tests, which was the aim of the homework task from Netguru.

### Approach to testing ###

The aim of this homework was to identify core features of a selected page, and write tests for them.
For the purpose of the task I chose to test https://dribbble.com/. Dribble is a social platform for digital designers. It serves as a portfolio platform, but also as a job recruiting site. Based on the acquired webpage profile I decided to focus on three functionalities (I know, it should be two but I couldn't help myself):

* Portfolio searching (users can search for projects shared on platform),
* Designers searching (users can search for specific designers and hire them),
* Job listing form (users can post a job announcement).

All tests were written for **not-logged in users**.

### Tests setup ###

Tests were written in Javascript, with the use of Cypress framework. Moreover Page Object Pattern was used for code maintainability and reusability.

Cypress is a desktop application installed on your computer. The desktop applications supports macOS, Linux, and Windows.

In order to run Cypress you have to run `npm install` in your root directory to fetch all dependencies.


After that you can use one of my prepared scripts depending on test kind:

* `npm run cy:run` to run all tests in headless Chrome,

* `npm run open-gui` to run all tests in Cypress GUI,

* `npm run designers` to run tests dedicated for Designers search functionality (3 tests),

* `npm run projects` to run tests dedicated for Portfolio search functionality (5 tests),

* `npm run job-form` to run tests dedicated for Job listing form functionality (3 tests).

#### Project structure ####

All Cypress tests can be found in ./cypress/integration/dribbble.

In ./cypress/fixtures you can find test data (used for form testing).

Page objects are in ./cypress/support/PageObjects.

### Important Note ###

During tests a bug was detected on a page. Issue is reported and attached to the email. While running tests you can notice that one of them doesn't pass. Don't worry - it means that tests are doing their job :)
### Contact ###

For further information please contact Natalia Cebula.